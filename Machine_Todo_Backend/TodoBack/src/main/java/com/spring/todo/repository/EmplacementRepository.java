package com.spring.todo.repository;

import com.spring.todo.entity.Emplacement;
import com.spring.todo.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmplacementRepository extends JpaRepository <Emplacement, Integer> {



}
