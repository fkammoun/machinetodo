package com.spring.todo.repository;

import com.spring.todo.entity.Emplacement;
import com.spring.todo.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository <Status, Integer> {



}
