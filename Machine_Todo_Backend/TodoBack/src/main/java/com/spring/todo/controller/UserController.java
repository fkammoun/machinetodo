package com.spring.todo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.spring.todo.entity.User;
import com.spring.todo.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;

@RestController()
@CrossOrigin
public class UserController {
	
	@Autowired
	private PasswordEncoder pe;

	@Autowired
	private UserRepository userRepository;

	@PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody User addUser(@RequestBody User newUser) {
		
		newUser.setPassword(pe.encode(newUser.getPassword()));
		return userRepository.save(newUser);
	}


	@GetMapping("/user")
	public @ResponseBody List<User> findAllTodo() {
		return userRepository.findAll();
	}

	@DeleteMapping(path = "/user/{id}")
	public void deleteUser(@PathVariable long id) {
		userRepository.deleteById(id);
	}

	@PutMapping (path = "/updateUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void UpdateUser(@RequestBody User user) {
		userRepository.save(user);
	}


}
