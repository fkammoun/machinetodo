package com.spring.todo.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "userr")
public class User implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
    @Column(name = "Name" ,nullable = false)
    private String name;
	
    @Column(name="password" ,nullable = false)
    private String password;
	
    @Column(name = "mail" ,nullable = false)
    private String mail;
	
    @Column(name = "roles" ,nullable = false)
    private String roles = "";


    public User(long id, String name, String password, String mail, String roles) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.mail = mail;
		this.roles = roles;
	}

	protected User(){}

    public long getId() {
        return id;
    }

  public String getMail() {
		return mail;
	}
  
    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

  
    public String getRoles() {
        return roles;
    }


    public List<String> getRoleList(){
        if(this.roles.length() > 0){
            return Arrays.asList(this.roles.split(","));
        }
        return new ArrayList<>();
    }

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

 
}