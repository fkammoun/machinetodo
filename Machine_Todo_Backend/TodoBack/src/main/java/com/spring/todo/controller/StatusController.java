package com.spring.todo.controller;

import com.spring.todo.entity.Emplacement;
import com.spring.todo.entity.Status;
import com.spring.todo.repository.EmplacementRepository;
import com.spring.todo.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@CrossOrigin
public class StatusController {

	@Autowired
	private StatusRepository statusRepository;

	@PostMapping(path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Status addStatus(@RequestBody Status status) {
		return statusRepository.save(status);
	}

	@PostMapping(path = "/updateStatus", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void updateStatus(@RequestBody Status status) {
		statusRepository.save(status);
	}

	@GetMapping("/status/{id}")
	public @ResponseBody Status findStatusById(@PathVariable int id) {
		if (statusRepository.findById(id).isPresent())
			return statusRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/status")
	public @ResponseBody List<Status> findAllStatus() {
		return statusRepository.findAll();
	}

	@DeleteMapping(path = "/status/{id}")
	public void deleteStatus(@PathVariable int id) {
		statusRepository.deleteById(id);
	}

}
