package com.spring.todo.controller;

import com.spring.todo.entity.Emplacement;
import com.spring.todo.entity.Machine;
import com.spring.todo.repository.EmplacementRepository;
import com.spring.todo.repository.MachineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@CrossOrigin
public class EmplacementController {

	@Autowired
	private EmplacementRepository emplacementRepository;

	@PostMapping(path = "/emplacement", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Emplacement addEmplacement(@RequestBody Emplacement newEmplacement) {
		return emplacementRepository.save(newEmplacement);
	}

	@PostMapping(path = "/updateEmplacement", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void updateEmplacement(@RequestBody Emplacement emplacement) {
		emplacementRepository.save(emplacement);
	}

	@GetMapping("/emplacement/{id}")
	public @ResponseBody Emplacement findById(@PathVariable int id) {
		if (emplacementRepository.findById(id).isPresent())
			return emplacementRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/emplacement")
	public @ResponseBody List<Emplacement> findAllEmplacement() {
		return emplacementRepository.findAll();
	}

	@DeleteMapping(path = "/emplacement/{id}")
	public void deleteEmplacement(@PathVariable int id) {
		emplacementRepository.deleteById(id);
	}

}
