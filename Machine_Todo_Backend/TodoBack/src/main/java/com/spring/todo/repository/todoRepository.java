package com.spring.todo.repository;

import com.spring.todo.entity.Emplacement;
import com.spring.todo.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.spring.todo.entity.Todo;

import java.util.List;

@Repository
public interface todoRepository extends JpaRepository <Todo, Integer> {

	@Query("select u from Todo u where u.machine.id = ?1")
	List<Todo> findAllByMachine(Integer id);

}
