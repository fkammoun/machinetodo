package com.spring.todo.controller;

import com.spring.todo.entity.Machine;
import com.spring.todo.entity.Todo;
import com.spring.todo.repository.MachineRepository;
import com.spring.todo.repository.todoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@CrossOrigin
public class MachineController {

	@Autowired
	private MachineRepository machineRepository;

	@PostMapping(path = "/machine", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Machine addMachine(@RequestBody Machine newMachine) {
		return machineRepository.save(newMachine);
	}

	@PostMapping(path = "/updateMachine", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void updateMachine(@RequestBody Machine machine) {
		machineRepository.save(machine);
	}

	@GetMapping("/machine/{id}")
	public @ResponseBody Machine findTodoById(@PathVariable int id) {
		if (machineRepository.findById(id).isPresent())
			return machineRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/machine")
	public @ResponseBody List<Machine> findAllMachine() {
		return machineRepository.findAll();
	}

	@DeleteMapping(path = "/machine/{id}")
	public void deleteMachine(@PathVariable int id) {
		machineRepository.deleteById(id);
	}

}
