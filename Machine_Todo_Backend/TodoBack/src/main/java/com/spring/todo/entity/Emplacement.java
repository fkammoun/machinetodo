package com.spring.todo.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "emplacement")
public class Emplacement implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "zone")
	private String zone;


	public int getId() {
		return id;
	}

	public String getZone() {
		return zone;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}
