export default  {

  /** GET_ALL_MACHINE. */ GET_ALL_MACHINE: `http://localhost:9000/machine`,
  /** GET_ALL_TODO_BY_MACHINE. */ GET_ALL_TODO_BY_MACHINE: `http://localhost:9000/todoByMachine`,
  /** GET_ALL_USERS. */ GET_ALL_USERS: `http://localhost:9000/user`,
  /** DELETE_USER. */ DELETE_USER: `http://localhost:9000/user/`,
  /** UPDATE_USER. */ UPDATE_USER: `http://localhost:9000/updateUser`,
  /** REGISTER. */ REGISTER: `http://localhost:9000/register`,
  /** GET_ALL_TODO_status. */ GET_ALL_STATUS: `http://localhost:9000/status`,
                              ADD_TODO: `http://localhost:9000/todo`

}

