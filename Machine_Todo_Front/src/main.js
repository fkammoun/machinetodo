/*!

 =========================================================
 * Vue Light Bootstrap Dashboard - v2.0.0 (Bootstrap 4)
 =========================================================

 * Product Page: http://www.creative-tim.com/product/light-bootstrap-dashboard
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard/blob/master/LICENSE.md)

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Vuelidate from "vuelidate";
import store from './store'

// LightBootstrap plugin
import LightBootstrap from './light-bootstrap-main'

// router setup
import routes from './routes/routes'

import './registerServiceWorker'
import { i18n } from '@/plugins/i18n'
import { Trans } from './plugins/Translation'
import Vuex from 'vuex'
import axios from 'axios';


Vue.use(Vuex)
Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)
// plugin setup
Vue.use(Vuelidate)

Vue.use(VueRouter)
Vue.use(LightBootstrap)

//Add request interceptors
axios.interceptors.request.use((config)=>{
  let token = localStorage.getItem('TOKEN');

    if (token) {
      config.headers['Authorization'] = `Bearer ${ token }`;
    }
  console.log('Request:',config);
  return config;
},
(error)=>{
})

//Add response interceptors
axios.interceptors.response.use((response)=>{
  console.log('response:',response.data);
  return response;
},
(error)=>{
  
})

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'nav-item active',
  mode: 'history',
  base: __dirname,
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
})



/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  store,
  render: h => h(App),
  router
  
})
