import DashboardLayout from '../layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../pages/NotFoundPage.vue'

// Admin pages
import Overview from 'src/pages/Overview.vue'
import UserProfile from 'src/pages/UserProfile.vue'
import TableList from 'src/pages/TableList.vue'
import Typography from 'src/pages/Typography.vue'
import Icons from 'src/pages/Icons.vue'
import Maps from 'src/pages/Maps.vue'
import Notifications from 'src/pages/Notifications.vue'
import Upgrade from 'src/pages/Upgrade.vue'
import Login from '../components/Authentification/Login/Login.vue'
import Register from '../components/Authentification/Register/Register.vue'
import Logout from '../components/Authentification/Logout/Logout.vue'
import ConsultUser from '../components/Profile/ConsultUser/ConsultUser.vue'
import MachineForm from  '../components/Machines/MachineForm.vue'
import MachineTodo from '../components/Machines/MachineTodo.vue'
import Users from '../components/Users/Users.vue'
import TodoForm from '../components/Todos/TodoForm'
import AddTodoForm from '../components/Todos/AddTodoForm.vue'
const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/admin/overview'
  },
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/overview',
    children: [
      {
        path: 'overview',
        name: 'Overview',
        component: Overview
      },
      {
        path: 'users',
        name: 'Users',
        component: Users
      },
      {
        path: 'consultUser',
        name: 'consultUser',
        component: ConsultUser
      },
      {
        path: 'login',
        name: 'login',
        component: Login
      },
      {
        path: 'logout',
        name: 'logout',
        component: Logout
      },
      {
        path: 'register',
        name: 'register',
        component: Register
      },
      {
        path: 'user',
        name: 'User',
        component: UserProfile
      },
      {
        path: 'table-list',
        name: 'Table List',
        component: TableList
      },
      {
        path: 'machine-form',
        name: 'Machine Form',
        component: MachineForm
      },
      {
        path: 'add-todo-form',
        name: 'AddTodoForm',
        component: AddTodoForm
      },
      {
        path: 'todo-form',
        name: 'TodoForm',
        component: TodoForm
      },
      {
        path: 'typography',
        name: 'Typography',
        component: Typography
      },
      {
        path: 'icons',
        name: 'Icons',
        component: Icons
      },
      {
        path: 'machine-todo',
        name: 'machine-todo',
        component: MachineTodo
      },
      {
        path: 'maps',
        name: 'Maps',
        component: Maps
      },
      {
        path: 'notifications',
        name: 'Notifications',
        component: Notifications
      },
      {
        path: 'upgrade',
        name: 'Upgrade to PRO',
        component: Upgrade
      }
    ]
  },

  { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
