import Vue from 'vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import http from "./environnement";
import sharedUrl from "./shared/constantes/sharedUrl";
import VueJwtDecode from 'vue-jwt-decode'
Vue.use(Vuex)
Vue.use(VueResource)

const state = {
  token: localStorage.getItem('TOKEN') || null ,
  roles:null,
  todo: {},
  user:'',
  machine:'',
  roles:''
}



const getters = {
  loggedIn(state) {
    return state.token !== null
  },
  token: state => state.token,

  tableData:state => state.tableData,

  todo: state => state.todo,

  roles(){
    if(state.token)
      return state.roles = VueJwtDecode.decode(localStorage.getItem('TOKEN')).sub.split(' ')[1]
  }
}

const mutations = {
  RETREIVE_TOKEN(state, TOKEN) {
    state.token = TOKEN
  },
  GET_TODO(state, TODO) {
    state.todo = TODO
  },
  GET_USER(state, USER) {
    state.user = USER
  },
  GET_MACHINE(state, MACHINE) {
    state.machine = MACHINE
  },
  DESTROY_TOKEN(state) {
    state.token = null
  },
  DECODE_TOKEN(state,ROLES){
    state.roles = ROLES
    
  }
}

const actions = {
   registerAction(context, data) {
    http.post(sharedUrl.REGISTER, {
      name: data.name,
      mail: data.mail,
      password: data.password,
      roles: data.roles
      
    })
      .then(response => {
        console.log(response)
      })
      .catch(error => {   
  })
},
decodeToken(context){
  
var roles=VueJwtDecode.decode(state.token).sub.split(' ')[1]
      context.commit('DECODE_TOKEN', roles)
},

  retrieveToken(context, credentials) {
    http.post('login', {
      mail: credentials.mail,
      password: credentials.password,
    })
      .then(response => {
        const token = response.headers.authorization.split(' ')[1]
        localStorage.setItem('TOKEN', token)
        console.log("decode token :",VueJwtDecode.decode(token).sub.split(' ')[1])
        context.commit('RETREIVE_TOKEN', token)
      })
      .catch(error => {
        console.log(error)
    })
},
  getTodo(context, TODO){

    context.commit('GET_TODO', TODO)

  },
  destroyToken(context) {
    http.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
    
    if (context.getters.loggedIn) {   
            localStorage.removeItem('TOKEN')
            context.commit('DESTROY_TOKEN')
          }
        },
  getMachine(context, MACHINE){

    context.commit('GET_MACHINE', MACHINE)

  }
}


let store = new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions,
  
})

export default store
